<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $idcategory
 * @property string $category
 */
class Category extends \yii\db\ActiveRecord
{

    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_category'], 'required'],
            [['nama_category'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcategory' => 'Idcategory',
            'nama_category' => 'Category',
        ];
    }

    public function getPosts()
    {
        return $this->hasMany(Post::class, ['idcategory' => 'idcategory']);
    }


}
