<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends Account implements \yii\web\IdentityInterface
{
    public $authKey;

    public static $users = [];

    // Metode untuk mengisi data dari database
    public static function loadUsersFromDatabase()
    {
        self::$users = self::find()->all();
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($username)
    {
        // return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return User::findOne(['username' => $username ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    } 

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['username' => $username ]);
    }

    public static function getIsAdmin()
    {
        $output = false;
        if(isset(Yii::$app->user->identity->role)){
            if(Yii::$app->user->identity->role == 'admin')
            $output = true;
        }
         
        return $output;
    }

    public static function getIsAuthor()
    {
        $output = false;
        if(isset(Yii::$app->user->identity->role)){
            if(Yii::$app->user->identity->role == 'author')
            $output = true;
        }
         
        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function getPosts()
    {
        return $this->hasMany(Post::class, ['username' => 'username']);
    }

    public function getRole() {
        return Yii::$app;
    }
}
