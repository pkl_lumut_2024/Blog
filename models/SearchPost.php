<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;
use Yii;

class SearchPost extends Post
{
    public $category;

    public function rules()
    {
        return [
            [['idpost'], 'integer'],
            [['title', 'content', 'date', 'username', 'category'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $forHome = false)
    {
        $query = Post::find()->joinWith(['category']);

        if ($forHome) {
            $query->andWhere(['status' => 'publish']);
        } else {
            $currentUser = Yii::$app->user->identity->username;
            $query->andWhere(['post.username' => $currentUser]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idpost' => $this->idpost,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'category.category', $this->category])
            ->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}
