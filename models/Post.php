<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "post".
 *
 * @property int $idpost
 * @property string $title
 * @property string $content
 * @property string $date
 * @property string $username
 * @property string $image
 * 
 *
 * @property Account $username0
 */
class Post extends \yii\db\ActiveRecord
{

    const STATUS_DRAFT = "draft";
    const STATUS_PUBLISH = "publish";

    public $file_image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'username', 'idcategory'], 'required'],
            [['title', 'content', 'image', 'status'], 'string'],
            [['date', 'date_publish'], 'safe'],
            [['username'], 'string', 'max' => 45],
            [['username'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['username' => 'username']],
            [['file_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
            [['status'], 'default', 'value' => self::STATUS_DRAFT],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpost' => 'Idpost',
            'title' => 'Title',
            'content' => 'Content',
            'date' => 'Date',
            'username' => 'Username',
            'image' => 'Cover',
            'idcategory' => 'Category',
            'status' => 'Status',
            'date_publish' => 'Date Publish'
        ];
    }

    /**
     * Gets query for [[Username0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
{
    return $this->hasOne(User::class, ['id' => 'user_id']);
}

    public function getCategory()
    {
        return $this->hasOne(Category::class, ['idcategory' => 'idcategory']);
    }
}
