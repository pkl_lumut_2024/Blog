<?php

namespace app\controllers;

use app\models\Post;
use app\models\SearchPost;
use app\models\User;
use app\models\Category;
use yii\web\UploadedFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        // 'actions' => ['index', 'view', 'create'],
                        'controllers' => ['post'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return User::getIsAdmin();
                        }
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'approve'],
                        // 'controllers' => ['post'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return User::getIsAuthor();
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchPost();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    //     $categories = Category::find()->all();
    //     return $this->render('ListCategory', [
    //         'categories' => $categories,
    //     ]);
    }


    // UNTUK MENAMPILKAN INDEX UNTUK ADMIN APPROVE 
    public function actionIndexApprove()
    {
        
        $query = Post::find();
        
         // Query untuk mengambil data post
         $query = Post::find();
         
             $query->andWhere(['status' => 'Draft']);

         // Data provider untuk digunakan di view
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
         ]);
        return $this->render('approve', [
            'dataProvider' => $dataProvider,
        ]);
    }


    // UNTUK MENGAPPROVE SALAH SATU BLOG
    public function actionApprove($idpost)
    {
        $model = $this->findModel($idpost);

        if (!User::getIsAdmin()) {
            throw new ForbiddenHttpException('Kamu Tidak Bisa Mengakses Aksi Ini');
        }
        
        $model->status = 'publish';
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Post has been published.');
        } else {
            Yii::$app->session->setFlash('error', 'Failed to approve the post.');
        }

        return $this->redirect(['index-approve']);
    }


    public function actionCategory($idcategory)
    {
        $category = Category::findOne($idcategory);

        if (!$category) {
            throw new \yii\web\NotFoundHttpException('Halaman Request Tidak Ada');
        }  

        $query = $category->getPosts();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('category', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    /**
     * Displays a single Post model.
     * @param int $idpost Idpost
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idpost)
    {
        return $this->render('view', [
            'model' => $this->findModel($idpost),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Post();
        

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {  

                $model->file_image = UploadedFile::getInstance($model, 'file_image');
                
            //    echo '<pre>';
            // print_r($model);
            // die;

            if($model->file_image){
                
                $img = uniqid('img_cover', true);
                $saveTo = 'uploads/cover/'.$img.'.'.$model->file_image->extension;
                if ($model->file_image->saveAs($saveTo, false));

                $model->image = $saveTo;
            }
            
                $model->save();
                return $this->redirect(['view', 'idpost' => $model->idpost]);
            }
        } else {
            $model->loadDefaultValues();
        }

        if (!Yii::$app->user->isGuest) {
            $model -> username = Yii::$app->user->identity->username;
        }
 
        $model -> date = date('Y-m-d H:i:s');

        $model -> date_publish = date('Y-m-d');
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idpost Idpost
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idpost)
    {
        $model = $this->findModel($idpost);

        if ($this->request->isPost && $model->load($this->request->post())) {

            $model->file_image = UploadedFile::getInstance($model, 'file_image');
            // echo '<pre>';
            // print_r($model);
            // die;

            if($model->file_image){
                
                $img = uniqid('img_cover', true);
                $saveTo = 'uploads/cover/'.$img.'.'.$model->file_image->extension;
                if ($model->file_image->saveAs($saveTo, false));

                $model->image = $saveTo;
            }

                    $model->save();

            return $this->redirect(['view', 'idpost' => $model->idpost]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idpost Idpost
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idpost)
    {
        $this->findModel($idpost)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idpost Idpost
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idpost)
    {
        if (($model = Post::findOne(['idpost' => $idpost])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
