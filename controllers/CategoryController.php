<?php

namespace app\controllers;

use app\models\Category;
use app\models\SearchCategory;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'ruleConfig' => [
                        'class' => AccessRule::className(),
                    ],
                    'rules' => [
                        [
                            'actions' => ['index', 'create', 'update', 'view'],
                        // 'controllers' => ['category'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return User::getIsAdmin();
                        }
                        
                        ],
                        [
                            // 'actions' => ['index', 'view', 'create'],
                            'controllers' => ['category'],
                            'allow' => true,
                            'matchCallback' => function($rule, $action) {
                                return User::getIsAuthor();
                            }
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Category models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SearchCategory();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        $categorys = Category::find()->all();
        return $this->render('index', [
        'categorys' => $categorys,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param int $idcategory Idcategory
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idcategory)
    {
        return $this->render('view', [
            'model' => $this->findModel($idcategory),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idcategory' => $model->idcategory]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idcategory Idcategory
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idcategory)
    {
        $model = $this->findModel($idcategory);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idcategory' => $model->idcategory]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idcategory Idcategory
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idcategory)
    {
        $this->findModel($idcategory)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idcategory Idcategory
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idcategory)
    {
        if (($model = Category::findOne(['idcategory' => $idcategory])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
