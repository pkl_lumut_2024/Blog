-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2024 at 05:08 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `username` varchar(45) NOT NULL,
  `password` varchar(250) NOT NULL,
  `name` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`username`, `password`, `name`, `role`) VALUES
('Ega', '12345678', 'Satria', 'admin'),
('Ega_2', '12345678', 'Fernada', 'author'),
('Udin', '12345', 'UdinPermei', 'author');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL,
  `nama_category` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idcategory`, `nama_category`) VALUES
(1, 'Olahraga'),
(2, 'Politik'),
(3, 'Animasi');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `idpost` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL,
  `username` varchar(45) NOT NULL,
  `image` varchar(225) NOT NULL,
  `idcategory` int(11) NOT NULL,
  `status` text NOT NULL,
  `date_publish` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`idpost`, `title`, `content`, `date`, `username`, `image`, `idcategory`, `status`, `date_publish`) VALUES
(5, 'Jajal Day 3', '<p><strong>When dealing with some complex data, it is possible that you may need to use multiple different models to collect the user input. For example, assuming the user login information is stored in the user table while the user profile information is stored in the profile table, you may want to collect the input data about a user through a User model and a Profile model. With the Yii model and form support, you can solve this problem in a way that is not much different from handling a single model.</strong></p>\r\n', '0000-00-00 00:00:00', 'Ega', 'uploads/cover/img_cover668cb1f12a8762.50982051.png', 1, 'Draft', '2024-07-18 00:00:00'),
(12, 'Gyahhhhhhhh', '<p>An attribute expression is an attribute name prefixed and/or suffixed with array indexes. It is mainly used in tabular data input and/or input of array type. Below are some examples:</p>\r\n', '2024-07-03 06:48:19', 'Udin', 'uploads/cover/img_cover668648ff7f9c54.10534841.jpg', 1, 'Draft', '2024-07-06 00:00:00'),
(13, 'Pemalsuan Tanda Tangan', '<p>Sebaiknya Teman Teman Hati Hati Karena Pemalsuan Tanda Tangan Rawan Terjadi</p>\r\n', '2024-07-03 18:31:29', 'Udin', 'uploads/cover/img_cover66864876338026.10648812.png', 2, 'Publish', '2024-07-21 00:00:00'),
(17, 'Day 11', '<p><strong>information to end user and to create&nbsp;<em>data</em>&nbsp;managing&nbsp;<em>UI</em>. A typical usage is as follows: use yii...: mixed, the key value associated with the&nbsp;<em>data</em>&nbsp;item. $index: integer, the zero-<em>based</em>&nbsp;index of the&nbsp;<em>data</em></strong></p>\r\n', '2024-07-11 07:40:49', 'Udin', 'uploads/cover/img_cover668f73f902f6c9.48033712.jpg', 2, 'Draft', '2024-07-16 00:00:00'),
(19, 'Jefri Nickol kolnya sayuran', '<p>Nama aslinya merupakan<strong>&nbsp;Jefry Nikhol,</strong> selaku salah satu contoh dari kecenderungan penamaan Minang yang unik. Ia merupakan anak pertama dari pasangan John Hendri dan Junita Eka Putri asal Sungai Geringging, Padang Pariaman. Jefri memiliki seorang adik perempuan bernama Jessie Putri.</p>\r\n', '2024-07-12 08:46:20', 'Ega', 'uploads/cover/img_cover6690d1ea688e92.07896130.png', 3, 'Draft', '2024-07-14 00:00:00'),
(20, 'Animasi 3D Dengan Istriii?!?!?!?', '<p>Hadehhh, Tobat Mass. <strong>Manusia Sudah Diciptakan Berpasang Pasangan.</strong> Tapi Kamu Malah Milih Yang 2D?? Dari<strong> 2 Milyar</strong> Wanita Didunia? Astagfirullah</p>\r\n', '2024-07-12 11:08:32', 'Ega', 'uploads/cover/img_cover6690f30257d249.29775421.jpg', 3, 'Publish', '2024-07-15 00:00:00'),
(21, 'fdfdg', '<p>dfdf</p>\r\n', '2024-07-16 04:18:04', 'Ega', '', 1, 'Draft', '2024-07-18 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`idpost`),
  ADD KEY `fk_post_account` (`username`),
  ADD KEY `idcategory` (`idcategory`),
  ADD KEY `idcategory_2` (`idcategory`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `idpost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `fk_post_account` FOREIGN KEY (`username`) REFERENCES `account` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_post_category` FOREIGN KEY (`idcategory`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
