<?php

/** @var yii\web\View $this */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\models\SearchPost;

/**  @var $posts app\models\Post[] */

$this->title = 'Beranda';
?>
<h1 style="text-align:center">Beranda</h1>



<div class="card" style="margin-bottom: 20px;">
    
    <div class="card-body">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model) {
            return '<div class="card h-100">' .

                    Html::img(Yii::$app->request->baseUrl . '/' . $model->image, ['class' => 'card-img-top', 'style' => 'width:100%;height:175px; object-fit:cover;']) .
                    '<div class="card-body">' .
                    '<h5 class="card-title">' . Html::encode($model->title) . '</h5>' .
                    '<p class="card-text">' . HtmlPurifier::process(($model->content), 100) . '</p>' .
                    '<p class="card-text"><small class="text-muted">Category: ' . Html::encode($model->category->nama_category) . '</small></p>' .
                    '<p class="card-text"><small class="text-muted">Diposting oleh: ' . Html::encode($model->username) . ' pada ' . Yii::$app->formatter->asDate($model->date) . '</small></p>' .
                    Html::a('View details &raquo;', ['post/view', 'idpost' => $model->idpost], ['class' => 'btn btn-primary']) .
                    '</div>' .
                    '</div>';
        },
        'itemOptions' => ['class' => 'col-lg-4 mb-4'],
        'layout' => "<div class='row'>{items}</div>\n<div class='pagination-wrapper text-center'>{pager}</div>",
    ]); ?>
        </div>
   
</div>


<style>
    .pagination-wrapper .pagination {
        display: inline-flex;
        justify-content: center;
        font-size: 1.2rem;
    }

    .pagination-wrapper .pagination .page-item {
        margin: 0 10px;
    }

    .pagination-wrapper .pagination .page-item a, 
    .pagination-wrapper .pagination .page-item span {
        border: 1px solid #ddd;
        padding: 10px 20px;
        text-decoration: none;
        color: #333;
        font-size: 1.5rem;
    }

    .pagination-wrapper .pagination .page-item a:hover,
    .pagination-wrapper .pagination .page-item span:hover {
        background-color: #007bff;
        color: white;
        border-color: #007bff;
    }

    .pagination-wrapper .pagination .active a, 
    .pagination-wrapper .pagination .active span,
    .pagination-wrapper .pagination .active a:hover, 
    .pagination-wrapper .pagination .active span:hover, 
    .pagination-wrapper .pagination .active a:focus, 
    .pagination-wrapper .pagination .active span:focus {
        background-color: #007bff;
        border-color: #007bff;
        color: white;
    }

    .site-index .card {
        font-size: 1.1rem;
    }

    .site-index .card-title {
        font-size: 1.5rem;
    }

    .site-index .card-text {
        font-size: 1.2rem;
    }

    .post-search .form-control {
        font-size: 1.2rem;
        padding: 10px;
    }

    .post-search .btn {
        font-size: 1.2rem;
        padding: 10px 20px;
    }
</style>
