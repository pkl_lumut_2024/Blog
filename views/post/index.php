<?php

use app\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\SearchPost;

/** @var yii\web\View $this */
/** @var app\models\SearchPost $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'idpost',
            'title:ntext',
            [
                'attribute' => 'content',
                'format' => 'html',
                'value' => function ($model) {
                    return ($model->content);
                },
            ],
            'date',
            'status',
            'date_publish',
            'username',
            'category.nama_category',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img(Yii::$app->request->baseUrl . '/' . $model->image, ['class' => 'img-responsive', 'style' => 'width:150px']);
                },
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Post $model) {
                    return Url::toRoute([$action, 'idpost' => $model->idpost]);
                 }
                ],
        ],
    ]); ?>


</div>
