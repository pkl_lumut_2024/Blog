<?php
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/** @var $this yii\web\View */
/** @var $category app\models\Category */
/** @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category' .' '. $category->nama_category;
$this->params['breadcrumbs'][] = ['label' => 'Categorys', 'url' => ['/views/post/category']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title)?></h1>

<?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model) {
            return
                    '<div class="card">' .
                        Html::img(Yii::$app->request->baseUrl . '/' . $model->image, ['class' => 'card-img-top', 'style' => 'width:100%;height:150px; object-fit:cover;']) .
                    '<div class="card-body">' .
                     '<h5 class="card-title">' . Html::encode($model->title) . '</h5>' .
                    '<p class="card-text">' . HtmlPurifier::process($model->content) . '</p>' .
                    // '<p class="card-text"><small class="text-muted">Kategori: ' . Html::encode($model->category->category) . '</small></p>' .
                    '<p class="card-text"><small class="text-muted">Diposting oleh: ' . Html::encode($model->username) . ' pada ' . Yii::$app->formatter->asDate($model->date) . '</small></p>' .
                    Html::a('View details &raquo;', ['post/view', 'idpost' => $model->idpost], ['class' => 'btn btn-primary']) .
                    '</div>' .
                    '</div>';
        },
        'itemOptions' => ['class' => 'col-lg-4 mb-4'],
        'layout' => "<div class='row'>{items}</div>\n{pager}",
    ]); ?>