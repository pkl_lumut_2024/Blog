<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Post;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;



/** @var yii\web\View $this */
/** @var app\models\Post $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
        'clientOptions' => [
        'allowedContent' => true,
    ],
    ]) ?>

    <?= $form->field($model, 'idcategory')->dropDownList(
    ArrayHelper::map(Category::find()->all(), 'idcategory', 'nama_category'),
    ['prompt' => '']) -> label('Category')?>

    <?= $form->field($model, 'file_image')->fileInput() ?>

    <?= $form->field($model, 'date')->hiddenInput() ->label(false) ?> 

    <?= $form->field($model, 'username')->hiddenInput(['maxlength' => true]) ->label(false) ?>

    <!-- <?= 
    $form->field($model, 'status')->dropDownList(
        ArrayHelper::map(Post::find()->select(['status'])->distinct()->all(), 'status', 'status'),
        ['prompt' => 'Select Status']) -> label(false) 
        ?> -->

    <?=
        $form->field($model, 'date_publish')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Masukkan Tanggal Publish'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>