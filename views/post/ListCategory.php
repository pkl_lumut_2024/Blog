<?php

use yii\grid\GridView;
use yii\widgets\ListView;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $category app\models\Category */
/** @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Category';
$this->params['breadcrumbs'][] = ['label' => 'Categorys', 'url' => ['/views/post/category']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ListCategory">
    <h1><?= Html::encode($this->title) ?></h1>
        <?= 
        GridView::widget([
            'category.nama_category'
        ])
        ?>
</div>